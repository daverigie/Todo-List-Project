
$(document).ready(function() {

// Check Off ToDo Item By Clicking
$("ul").on("click", "li", function(){
	$(this).toggleClass("completed");
});

// Check Off ToDo Item By Clicking
$("ul").on("click", "span", function(e){
	$(this).parent().fadeOut(500,function(){
			this.remove();
		});
	e.stopPropagation();
})

// Add to Item
$("input[type='text']").keypress(function(e){
	if(e.which === 13){
		// Get text from input
		var todoText = $(this).val();
		$(this).val("");
		// Create new LI and add to ul
		$("ul").append("<li><span> <i class='fa fa-trash'></i> </span>" + todoText + "</li>")
	}
		
});

$(".fa-plus").click(function(){
	$("input[type='text']").fadeToggle(100);
})

});

